import 'package:flutter/material.dart';
import 'package:flutter_note_app/helpers/database_helper.dart';
import 'package:flutter_note_app/models/note_model.dart';

class AddNoteScreen extends StatefulWidget {
  final Note note;
  final Function updateNoteList;
  final bool isLandscape;

  AddNoteScreen({this.note, this.updateNoteList, this.isLandscape});

  @override
  _AddNoteScreenState createState() => _AddNoteScreenState();
}

class _AddNoteScreenState extends State<AddNoteScreen> {
  final _formKey = GlobalKey<FormState>();
  String _title = '';
  String _subtitle = '';

  @override
  void initState() {
    super.initState();
    if (widget.note != null) {
      _title = widget.note.title;
      _subtitle = widget.note.subtitle;
    }
  }

  _delete() {
    DatabaseHelper.instance.deleteNote(widget.note.id);
    widget.updateNoteList();
    if (!widget.isLandscape) {
      Navigator.pop(context);
    }
  }

  _submit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      print(_title);

      //insert
      Note note = Note(title: _title, subtitle: _subtitle);
      if (widget.note == null) {
        DatabaseHelper.instance.insertNote(note);
      } else {
        note.id = widget.note.id;
        DatabaseHelper.instance.updateNote(note);
      }

      //update
      widget.updateNoteList();
      if (!widget.isLandscape) {
        Navigator.pop(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                !widget.isLandscape
                    ? GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios,
                          size: 30,
                          color: Theme.of(context).accentColor,
                        ),
                        onTap: () => Navigator.pop(context),
                      )
                    : SizedBox.shrink(),
                SizedBox(
                  height: 20,
                ),
                Text(
                  widget.note == null ? "Add Note" : "Edit note",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "PlayfairDisplay",
                      fontSize: 40),
                ),
                SizedBox(
                  height: 10,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: TextFormField(
                          style: TextStyle(
                            fontSize: 20,
                            fontFamily: "PlayfairDisplay",
                          ),
                          decoration: InputDecoration(
                            labelText: "Title",
                            labelStyle: TextStyle(fontSize: 20),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          validator: (input) => input.trim().isEmpty
                              ? "please enter a note title"
                              : null,
                          onSaved: (input) => _title = input,
                          initialValue: _title,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: TextFormField(
                          maxLines: 15,
                          style: TextStyle(fontSize: 20),
                          decoration: InputDecoration(
                            labelStyle: TextStyle(fontSize: 20),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          validator: (input) => input.trim().isEmpty
                              ? "please enter a note content"
                              : null,
                          onSaved: (input) => _subtitle = input,
                          initialValue: _subtitle,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        height: 60,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: TextButton(
                            onPressed: _submit,
                            child: Text(
                              widget.note == null ? "ADD" : "Edit",
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 20),
                            )),
                      ),
                      widget.note != null
                          ? Container(
                              margin: EdgeInsets.symmetric(vertical: 20),
                              height: 60,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(30)),
                              child: TextButton(
                                onPressed: _delete,
                                child: Text(
                                  "DELETE",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ),
                            )
                          : SizedBox.shrink()
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
