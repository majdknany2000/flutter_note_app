import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_note_app/helpers/database_helper.dart';
import 'package:flutter_note_app/models/note_model.dart';
import 'package:flutter_note_app/screens/add_edit_note_screen.dart';
import 'package:flutter_note_app/screens/add_note_Screen.dart';

class NoteListScreen extends StatefulWidget {
  @override
  _NoteListScreenState createState() => _NoteListScreenState();
}

class _NoteListScreenState extends State<NoteListScreen> {
  Note _note;

  Future<List<Note>> _noteList;

  @override
  void initState() {
    super.initState();
    _updateNoteList();
  }

  _updateNoteList() {
    setState(
      () {
        _noteList = DatabaseHelper.instance.getNoteList();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 600) {
          return WideNoteListScreen(context);
        } else {
          return NarrowNoteListScreen(context);
        }
      },
    );
  }

  // ignore: non_constant_identifier_names
  Scaffold WideNoteListScreen(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          Expanded(
            child: body(wide: true),
            flex: 2,
          ),
          VerticalDivider(
            color: Theme.of(context).accentColor,
          ),
          Expanded(
            child: _note != null
                ? Container(
                    child: AddEditNoteScreen(
                      updateNoteList: _updateNoteList,
                      isLandscape: true,
                      note: _note,
                    ),
                  )
                : AddNoteScreen(
                    isLandscape: true,
                    updateNoteList: _updateNoteList,
                  ),
            flex: 3,
          )
        ],
      ),
    );
  }

  Scaffold NarrowNoteListScreen(BuildContext context) {
    return body(wide: false);
  }

  FloatingActionButton FAB(BuildContext context, bool wide) {
    return FloatingActionButton.extended(
      backgroundColor: Theme.of(context).primaryColor,
      icon: Icon(
        Icons.add,
        color: Theme.of(context).accentColor,
      ),
      onPressed: !wide
          ? () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) {
                    return AddNoteScreen(
                      isLandscape: false,
                      updateNoteList: _updateNoteList,
                    );
                  },
                ),
              )
          : () {
              setState(
                () {
                  _note = null;
                },
              );
            },
      label: Text(
        "ADD",
        style: TextStyle(color: Theme.of(context).accentColor),
      ),
    );
  }

  Scaffold body({bool wide}) {
    return Scaffold(
      body: FutureBuilder(
          future: _noteList,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }
            return noteList(snapshot, wide);
          }),
      floatingActionButton: FAB(context, wide),
    );
  }

  ListView noteList(AsyncSnapshot<dynamic> snapshot, bool wide) {
    return ListView.builder(
      padding: EdgeInsets.symmetric(vertical: 40),
      itemCount: 1 + snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            child: Text(
              "My Notes",
              style: TextStyle(
                  fontFamily: "PlayfairDisplay",
                  fontSize: 40,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          );
        }

        return _buildNote(
          snapshot.data[index - 1],
          () {
            setState(
              () {
                _note = snapshot.data[index - 1];
              },
            );
            print(snapshot.data[index - 1].title);
            print("${_note.title}...");
          },
          wide,
        );
      },
    );
  }

  Widget _buildNote(Note note, Function onNoteTap, bool wide) {
    return Padding(
      padding: EdgeInsets.only(left: 0, bottom: 0, right: 35, top: 25),
      child: GestureDetector(
        onTap: !wide
            ? () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => AddNoteScreen(
                        isLandscape: false,
                        updateNoteList: _updateNoteList,
                        note: note),
                  ),
                );
              }
            : onNoteTap,
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: 0.0),
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(0),
                  topLeft: Radius.circular(0))),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  note.title,
                  style: TextStyle(
                      fontFamily: "PlayfairDisplay",
                      fontSize: 24,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  note.subtitle,
                  maxLines: 7,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 15, color: Colors.black87),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
