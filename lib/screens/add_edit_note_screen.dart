import 'package:flutter/material.dart';
import 'package:flutter_note_app/helpers/database_helper.dart';
import 'package:flutter_note_app/models/note_model.dart';

class AddEditNoteScreen extends StatefulWidget {
  final Note note;
  final Function updateNoteList;
  final bool isLandscape;
  AddEditNoteScreen({this.note, this.updateNoteList, this.isLandscape});
  @override
  _AddEditNoteScreenState createState() => _AddEditNoteScreenState();
}

class _AddEditNoteScreenState extends State<AddEditNoteScreen> {
  TextEditingController titleController = TextEditingController(text: "");
  @override
  void initState() {
    super.initState();
    if (widget.note != null) titleController.text = widget.note.title;
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController titleController =
        TextEditingController(text: widget.note.title);
    TextEditingController subtitleController =
        TextEditingController(text: widget.note.subtitle);
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                !widget.isLandscape
                    ? GestureDetector(
                        child: Icon(
                          Icons.arrow_back_ios,
                          size: 30,
                          color: Theme.of(context).accentColor,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      )
                    : SizedBox(),
                SizedBox(
                  height: 20,
                ),
                Text(
                  widget.note == null ? "Add Note" : "Edit Note",
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: "PlayfairDisplay",
                    fontSize: 40,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: TextField(
                    style: TextStyle(
                      fontSize: 20,
                      fontFamily: "PlayfairDisplay",
                    ),
                    decoration: InputDecoration(
                      labelText: "Title",
                      labelStyle: TextStyle(fontSize: 20),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    controller: titleController,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: TextField(
                    maxLines: 15,
                    style: TextStyle(fontSize: 20),
                    decoration: InputDecoration(
                      labelStyle: TextStyle(fontSize: 20),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    controller: subtitleController,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: TextButton(
                    onPressed: () {
                      //insert
                      Note note = Note(
                          title: titleController.text,
                          subtitle: subtitleController.text);
                      print(note.title);
                      if (widget.note == null) {
                        DatabaseHelper.instance.insertNote(note);
                      } else {
                        note.id = widget.note.id;
                        DatabaseHelper.instance.updateNote(note);
                      }
                      //update
                      widget.updateNoteList();
                      if (!widget.isLandscape) {
                        Navigator.pop(context);
                      }
                    },
                    child: Text(
                      widget.note == null ? "ADD" : "EDIT",
                      style: TextStyle(
                          color: Theme.of(context).accentColor, fontSize: 20),
                    ),
                  ),
                ),
                widget.note != null
                    ? Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        height: 60,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: TextButton(
                          onPressed: () {
                            DatabaseHelper.instance.deleteNote(widget.note.id);
                            widget.updateNoteList();
                            if (!widget.isLandscape) {
                              Navigator.pop(context);
                            }
                          },
                          child: Text(
                            "DELETE",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        ),
                      )
                    : SizedBox.shrink()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
