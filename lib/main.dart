import 'package:flutter/material.dart';
import 'package:flutter_note_app/screens/note_list_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Flutter Note app',
      theme: ThemeData(
        accentColor: Colors.red,
        primarySwatch: Colors.blueGrey,
      ),
      home: NoteListScreen(),
    );
  }
}
