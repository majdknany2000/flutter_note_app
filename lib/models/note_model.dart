class Note {
  int id;
  String title;
  String subtitle;

  Note({
    this.title,
    this.subtitle,
  });

  Note.withId({
    this.id,
    this.title,
    this.subtitle,
  });

  Map<String, dynamic> toMap() {
    final map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['title'] = title;
    map['subtitle'] = subtitle;
    return map;
  }

  factory Note.fromMap(Map<String, dynamic> map) {
    return Note.withId(
      id: map['id'],
      title: map['title'],
      subtitle: map['subtitle'],
    );
  }
}
